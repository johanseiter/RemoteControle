package de.homes.remotecontrole;

import android.os.AsyncTask;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * Created by js on 21.11.2017.
 */

public class MakeNetworkCall  extends AsyncTask<String, Void, String> {

    @Override
    protected String doInBackground(String... strings) {
        try {
        OkHttpClient client = new OkHttpClient();
        String url = new String(strings[0]);
        System.out.println(strings[0]);

        Request request = new Request.Builder()
                .url("http://www.vogella.com/index.html")
                .build();

            client.newCall(request).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
