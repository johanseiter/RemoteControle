package de.homes.remotecontrole;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Switch;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;


public class UEBERSICHT_Fragment extends Fragment implements CompoundButton.OnCheckedChangeListener {

    public static final String ARG_PAGE = "ARG_PAGE";
    private int mPage;
    Switch s1, s2,s3,s4,s5;
    private ListView mListView;
    private OnFragmentInteractionListener mListener;
    OkHttpClient client = new OkHttpClient();

    public static UEBERSICHT_Fragment newInstance(int page, String title) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        UEBERSICHT_Fragment fragment = new UEBERSICHT_Fragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {



        View view = inflater.inflate(R.layout.fragment_uebersicht, container, false);

        s1 = (Switch) view.findViewById(R.id.switch1);
        s2 = (Switch) view.findViewById(R.id.switch2);
        s3 = (Switch) view.findViewById(R.id.switch3);
        s4 = (Switch) view.findViewById(R.id.switch4);
        s5 = (Switch) view.findViewById(R.id.switch5);

        s1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    MakeNetworkCall makeNetworkCall = new MakeNetworkCall();
                    makeNetworkCall.execute("");
                }
                else{
                    MakeNetworkCall makeNetworkCall = new MakeNetworkCall();
                    makeNetworkCall.execute("");
                }
            }
        });

        s2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    MakeNetworkCall makeNetworkCall = new MakeNetworkCall();
                    makeNetworkCall.execute("");
                }
                else{
                    MakeNetworkCall makeNetworkCall = new MakeNetworkCall();
                    makeNetworkCall.execute("");
                }
            }
        });

        s3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    MakeNetworkCall makeNetworkCall = new MakeNetworkCall();
                    makeNetworkCall.execute("");
                }
                else{
                    MakeNetworkCall makeNetworkCall = new MakeNetworkCall();
                    makeNetworkCall.execute("");
                }
            }
        });

        s4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    MakeNetworkCall makeNetworkCall = new MakeNetworkCall();
                    makeNetworkCall.execute("");
                }
                else{
                    MakeNetworkCall makeNetworkCall = new MakeNetworkCall();
                    makeNetworkCall.execute("");
                }
            }
        });

        s5.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    MakeNetworkCall makeNetworkCall = new MakeNetworkCall();
                    makeNetworkCall.execute("");
                }
                else{
                    MakeNetworkCall makeNetworkCall = new MakeNetworkCall();
                    makeNetworkCall.execute("");
                }
            }
        });



        // Inflate the layout for this fragment
        return view;

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


}
